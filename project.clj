(defproject mmep "0.1.0-SNAPSHOT"
  :license {:name "WTFPL"
            :url "www.wtfpl.net/"}
  :dependencies [[org.clojure/clojure "1.7.0"][org.clojure/math.numeric-tower "0.0.4"]]
  :main ^:skip-aot mmep.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
