# maximal-modular-exponent-pairs (mmep)

Let f(x,y) = (x^y) % (x+y).

Given a list L of whole integers, what is the maximum value that can be obtained under the following conditions:

1.Each member of L can be used in f(x,y) one single time, as either argument.

2.Values returned from f(x,y) are added to L, and themselves can be used under condition 1.

3.The value to be maximized is the sum of all values in L.
  
Observations:

The maximum number of integers that can be obtained from this process is 2|L| - 1.

This is apparent when understanding that the maximum number of operations performable is |L|-1,
as given |L|-1 operations we'll have used 2|L|-2 elements with one single pairable element remaining.

Furthermore, there is never a reason not to obtain all 2[L] -1 integers. All results of f(x) will be whole on
account of |L| containing whole numbers itself; whole numbers to a given power will still be whole and
modular division will keep them whole. It might not help us, and we may just get a long string of zeroes,
but our result is never harmed by performing all operations possible.

The maximum value that can be obtained from any pairing f(x,y) is x+y-1 by the application of modular division. 

The maximum potential value from any L would be if each successive pairing resulted in something greater than
everything in the list which was x+y-1 where x and y were the maximum values after the previous pairing. 

Any pairing containing two ns will result in 0 if n is even and n if n is odd.

Any pairing containing one 0 will result in 1, should the obvious 0^n be avoided.

Any pairing containing 1 and n will result in n, should the obvious 1^n be avoided.

Given L, what is our best pairing?


Base case 1:

Suppose we have a single pair available.

Obviously our best pairing is the pairing which has the maximum result. 
This happens to also be the pairing, given a single pair, which is closest to all potential pairs' potential maximum (x+y).

Suppose we have two pairs available.

At a basic level our best pairing is whatever pairing gives us a better maximum result, given base case 1 on the result.

This is -not- necessarily the pairing which has the maximum result, by counter-example: [2,3,5]

The best result is [2,3,4,5,7], generated from pairing [3,2] and then [4,5].

But the maximum at [2,3,5] is to pair [5,3], which begets 5 instead of 4. [2,5] then begets 4 for [2,3,4,5,5].

[3,2] is, however, the closest pairing to it's potential maximum, as 4 = 3 + 2 - 1.

Always choosing the closest pairing to it's potential maximum does not always get us the best result either.

As a counter example: [2, 3, 4, 5, 6].

Taking a greedy approach and picking the biggest value each time here will beget 54, from [4, 4, 8, 2, 12, 10, 3, 6, 5].

Taking the closest pairing to it's potential maximum each time, even when breaking ties by greatest value, begets 48, from [0, 4, 12, 2, 4, 10, 3, 6, 5].

This implementation attempts both approaches, tie breaking on the opposing approach, at each branch. It returns whichever series of approaches  produced the highest sum from each branch. On top of these approaches it uses a percentage-based, mixed approach, choosing whichever has a difference closest to x+y relative to how large x+y is, choosing a difference of 10 when x+y = 12 before 2, when x+y = 3.

This approach does not search all possible trees of choices, and if it is ever correct to not perform one of the three actions above, it will choose wrong.

This program offers a basic implementation, where an action must be chosen at each branch and values are constant, and also a bonus implementation which reduces values every generation and allows for the program to choose not to act. The value to be maximized here is the total value of all numbers ever seen, excluding those used in a pairing in a given generation.

## Usage

lein run [mode] [filename | integer list]

## Options

[mode] - basic or bonus

[filename] - a string representing a path to a .txt file with space-delimited integers and new-line delimited input sets.

[integer list] - a series of integers (candy cane plants?)

## Examples

lein run basic 2 3 5 

---[7 4 5 3 2]

---21

lein run bonus 2 3 5

---Total production value = 40

lein run basic test.txt

Without lein:

java -jar target/uberjar/mmep-0.1.0-SNAPSHOT-standalone.jar bonus test.txt

## Todo

Further improve efficiency of the program. Currently will be unlikely to finish a list of over twenty-six entities in a reasonable time for the basic implementation, and fifteen entities in the bonus implementation or entities over several thousand in the bonus implementation.

Prove or disprove that the given approach will return the maximum value in all instances.

There exists in mmep.math an algorithm which returns the maximum potential value from a given vector of integers, for the basic implementation. For it to be inserted into the basic implementation and used to prune trees that will never exceed old branches' value, the implementation needs to be modified to hold onto its best known value so far, which will require it also returning that value. 

## License

WTFPL