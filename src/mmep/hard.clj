(ns mmep.hard
      (:gen-class))
(use '[mmep.math])
(use '[mmep.comp])
(use '[mmep.iter])
(declare mmep_hard)

; bestVal - the best value from all previous generations.
;               - held onto to allow for tail recursion.
(defn mmep_hard_recurse [pairs sum bestVal]
  (let [skip_gen_sum (+ sum (reduce + pairs))
          skip_gen_pairs (vec (doall (map dec_max_0 pairs)))]
  (if (= sum skip_gen_sum)
      (max sum bestVal)
      (recur skip_gen_pairs skip_gen_sum (max 
        (mmep_hard pairs mod_diff_comp sum)
        (mmep_hard pairs max_val_comp sum)
        (mmep_hard pairs percent_mod_diff_comp sum) 
        bestVal)))))

; Given a set of integers and a function to compare results,
; consider all pairs of integers possible to form in the set
; and compare them to some quality of value from their
; x^y % x+y value back, based on the comparator function.
; Add that value to the initial list, removing the values that
; were used to generate it. Accrue value, reduce the lists' values,
; and recurse.
(defn mmep_hard [pairs tieBreak sum]
  (if (< (count pairs) 2)
    ; Default case
    ; A lone plant cannot breed any longer, so its future value is calculatable.
    ( + sum (fut_prod (first pairs)))
    (let [toPop (mmep_allPairs pairs 0 tieBreak)
        ; Extract some elements from the returned value
        ; and shorthand some repeated references to them. 
        xIndex (second toPop)
        yIndex (last toPop)
        splitIndex1 (min xIndex yIndex)
        splitIndex2 (max xIndex yIndex)
        x (nth pairs xIndex)
        y (nth pairs yIndex)
        ; Remove from the next list the two
        ; numbers that were paired in this generation
        ; while adding their child. 
        nextPairs (vec (concat 
          (subvec pairs 0 splitIndex1)
          (subvec pairs (inc splitIndex1) splitIndex2)
          (subvec pairs (inc splitIndex2))))
        ; In this generation, we accumulated the value of
        ; the future value in those we bred (whose value is
        ; no longer variable)
        ; and what every other population member produced.
        pairSum (+  (fut_prod (dec_max_0 y)) (fut_prod (dec_max_0 x)) (reduce + nextPairs))
        ; Reduce the old guard by one and add the new guy on the end. 
        reducedPairs (vec (concat
          (vec (doall (map dec_max_0 nextPairs))) 
          (vector (mod_expt_pair_sum x y))))]
      (mmep_hard_recurse reducedPairs (+ pairSum sum) 0))))  

(def mmep_hard (memoize mmep_hard))

(defn mmep_hard_init [pairs]
  (println (str "Calculating maximized list from " pairs))
  (println (str "Total production value = " (int (mmep_hard_recurse pairs 0 0)))))