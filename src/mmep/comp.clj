(ns  mmep.comp
  (:gen-class))
(use '[mmep.math])

;********
; Tie break and result comparison functions

; Choose the result with the lower mod diff, and 
; break ties on higher value.
(defn mod_diff_comp [vec1 vec2]
  (if (= (first (first vec1)) (first (first vec2)))
    (if (> (second (first vec1)) (second (first vec2)))
      vec1 vec2)
    (if (< (first (first vec1)) (first (first vec2)))
      vec1 vec2)))

; Choose the result with the proportionally lower
; mod diff relative to the pair's value
(defn percent_mod_diff_comp [vec1 vec2]
  (let [diff1 (first (first vec1))
           diff2 (first (first vec2))
           val1 (second (first vec1))
           val2 (second (first vec2))
           percent1 (safe_div diff1 (+ diff1 val1))
           percent2 (safe_div diff2 (+ diff2 val2))]
    (if (= percent1 percent2)
      (if (> val1 val2)
          vec1 vec2)
      ; / inf (+ inf n-inf) is NaN, and numbers aren't less than NaN
      (if (Double/isNaN percent2) 
        vec1
        (if (< percent1 percent2)
          vec1 vec2)))))

; Choose the result with the higher value,
; and break ties on lower mod diff.
(defn max_val_comp [vec1 vec2]
  (if (= (second (first vec1)) (second (first vec2)))
    (if (< (first (first vec1)) (first (first vec2)))
      vec1 vec2)
    (if (> (second (first vec1)) (second (first vec2)))
      vec1 vec2)))

; Choose the flat collection of integers which has a greater sum
(defn max_sum_comp [vec1 vec2 vec3]
  (let [sum1 (reduce + vec1)
           sum2 (reduce + vec2)
           sum3 (reduce + vec3)]
    (if (> sum1 sum2)
      (if (> sum1 sum3)
        vec1
        (if (> sum2 sum3)
          vec2
          vec3))
      (if (> sum2 sum3)
        vec2
        (if (> sum1 sum3)
          vec1
          vec3)))))