(ns mmep.iter
 (:gen-class))
(use '[mmep.math])

;**********
; Pair iterators

; Return the minimum value of the exponential modular differences
; of one element against the whole of a list.
; Pair that value with the integers used to get it.
(defn mmep_bestPair [x yList xIndex relYIndex tieBreak]
  ; Compare x^y % x+y to y^x % x+y
  (let [curResult (tieBreak 
      ; Returned values take the form of 
      ; [[mod-diff mod-expt-val] base-index exponent-index]
      (vector
        (vector
          (mod_expt_mod_diff x (first yList))
          (mod_expt_pair_sum x (first yList)))
        xIndex
        (+ xIndex relYIndex))
      (vector
        (vector
          (mod_expt_mod_diff (first yList) x)
          (mod_expt_pair_sum (first yList) x))
        (+ xIndex relYIndex)
        xIndex))]
    (if (empty? (rest yList))
      curResult
      (tieBreak curResult
            (mmep_bestPair x (rest yList) xIndex (inc relYIndex) tieBreak)))))

(def mmep_bestPair (memoize mmep_bestPair))

; Perform mmep_bestPair over all possible pairs in a given integer list
(defn mmep_allPairs [pairs curIndex tieBreak]
  (if (empty? (rest pairs))
    ; Return the worst possible pairing so it is replaced by other branches.
    (vector [inf n-inf] 0 0)
    (tieBreak (mmep_bestPair (first pairs) (rest pairs) curIndex 1 tieBreak)
          (mmep_allPairs (rest pairs) (inc curIndex) tieBreak))))