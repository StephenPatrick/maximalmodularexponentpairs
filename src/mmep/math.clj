(ns mmep.math
      (:gen-class))
      
; ********
; Core Math functions

(def inf Double/POSITIVE_INFINITY)
(def n-inf Double/NEGATIVE_INFINITY)

; Fast modular exponentiation. 
(defn mod_expt_rec [result x y modulus]
  (if (< y 1)
    result
    (mod_expt_rec
      (if (= (mod y 2) 1)
        (mod (* result x) modulus)
        result)
      (mod (* x x) modulus)
      (bit-shift-right y 1)
      modulus)))

(def mod_expt_rec (memoize mod_expt_rec))

; Division safe from division by zero
(defn safe_div [x y]
  (if (= y 0)
    inf
    (/ x y)))

; What is x^y % x+y?
; x and y are assumed to be whole
(defn mod_expt_pair_sum [x y]
  (let [modulus (+ x y)]
    (if (<= modulus 1)
      0
      (mod_expt_rec 1 (mod x modulus) y modulus))))

; What is the difference between x+y and x^y % x+y?
; x and y are assumed to be whole
(defn mod_expt_mod_diff [x y]
  (- (+ x y) (mod_expt_pair_sum x y)))

; How much does a non-breedable plant produce for the rest of it's lifespan?
(defn fut_prod [x]
  (* x (+ x 1) 0.5))

; Decrease a number by one unless that puts it below zero.
(defn dec_max_0 [x]
  (max 0 (dec x)))

; Given a vector of integers, suppose it returns the max value possible
; for each pairing of x and y (x+y-1) and each x and y pairing are the two highest values
; of the existing vector.
; (Proof not given) this is equal to -n[|n|-1] + 
;                                                           0*(n[0]-1)+2n[0] +
;                                                           1*(n[1]-1)+2n[1]+
;                                                           2*(n[2]-1)+2n[2]...
; For pruning
(defn max_potential [vec1]
  (let [max_sort (sort vec1)]
  (- (reduce + (map-indexed 
                            (fn [idx itm] (+ (* idx (dec itm)) (* 2 itm)))
                            max_sort))
     (last max_sort))))