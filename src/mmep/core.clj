; m m e p - Maximum Modual Exponent Pairs
(ns mmep.core
  (:gen-class))
(use '[mmep.hard])
(use '[mmep.math])
(use '[mmep.comp])
(use '[mmep.iter])
(use ['clojure.java.io])
(use '[clojure.string :only (split)])
(declare mmep)

; At each branch, choose whatever result has more production 
; from either of the approaches
(defn mmep_recurse [pairs x y]
  (conj (max_sum_comp
    (mmep pairs mod_diff_comp)
    (mmep pairs max_val_comp)
    (mmep pairs percent_mod_diff_comp)) x y))

; Given a set of integers and a function to compare results,
; consider all pairs of integers possible to form in the set
; and compare them to some quality of value from their
; x^y % x+y value back, based on the comparator function.
; Add that value to the initial list, removing the values that
; were used to generate it, and recurse. 
(defn mmep [pairs tieBreak]
  (if (< (count pairs) 2)
    ; Default case
    pairs
    (let [toPop (mmep_allPairs pairs 0 tieBreak)
        ; Extract some elements from the returned value
        ; and shorthand some repeated references to them. 
        xIndex (second toPop)
        yIndex (last toPop)
        splitIndex1 (min xIndex yIndex)
        splitIndex2 (max xIndex yIndex)
        x (nth pairs xIndex)
        y (nth pairs yIndex)
        ; Remove from the next list the two
        ; numbers that were paired in this generation
        ; while adding their child. 
        nextPairs (vec (concat
        ; Place the new element at the beginning of the vector
        ; So that mmep_bestPair is better memoized, with less
        ; argument lists of predictably new arguments 
        ; exposed to it.
        (vector (mod_expt_pair_sum x y)) 
        (subvec pairs 0 splitIndex1)
        (subvec pairs (inc splitIndex1) splitIndex2)
        (subvec pairs (inc splitIndex2))))]
      (mmep_recurse nextPairs x y))))

(def mmep (memoize mmep))

; Initialization
(defn mmep_init [pairs]
  (println (str "Calculating maximized list from " pairs))
  (let [res (mmep_recurse pairs 0 0)
          reduced_res (subvec res 0 (- (count res) 2))]
    (println (str "Maximized list = " reduced_res))
    (println (str "List sum = " (reduce + reduced_res)))))

; Line-by-line reading, given a file.
(defn mmep_file [func filename]
  (with-open [file_reader (reader filename)]
    (doseq [line (line-seq file_reader)]
      (func (vec (map #(Integer/parseInt %) (split line #" ")))))))

(defn -main [& args]
  (time
    ; If the input should be run on the bonus problem.
    (if (= (first args) "bonus")
      ; If the second argument is a file with extension .txt
      (if (nil? (re-matches #".*\.txt" (second args)))
        (mmep_hard_init (vec (map #(Integer/parseInt %) (rest args))))
        (mmep_file mmep_hard_init (second args)))
      (if (nil? (re-matches #".*\.txt" (second args)))
        (mmep_init (vec (map #(Integer/parseInt %) (rest args))))
        (mmep_file mmep_init (second args))))))